angular.module('starter.controllers', [])

    .controller('ContactsCtrl', function ($scope, Contacts) {
        $scope.contacts = Contacts.all();
    })

    .controller('AddContactsCtrl', function ($scope, $state, Contacts) {

        $scope.save = function(form, input) {
            if(form.$valid) {
                Contacts.add(input);
                $state.go('tab.contacts');
            }
        }

    })

    .controller('DetailContactsCtrl', function ($scope, $stateParams, $state, $ionicModal, Contacts) {
        $scope.contact = Contacts.get($stateParams.contactId);

        $scope.removeModal = function () {
            //$scope.idToRemove = index;
            $ionicModal.fromTemplateUrl('templates/contacts/remove-modal.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };

        $scope.remove = function() {
            Contacts.remove($scope.contact.id);
            $scope.closeModal();
            $state.go('tab.contacts');//, {reload: true});
        };

        $scope.closeModal = function () {
            $scope.modal.hide();
            $scope.modal.remove()
        };
    })

    .controller('EditContactsCtrl', function ($scope, $state, $stateParams, Contacts) {
        $scope.formData = Contacts.get($stateParams.contactId);
        $scope.save = function(form, intput) {
            //console.log($scope.formData);
            if(form.$valid) {
                Contacts.update($scope.formData);
                $state.go('tab.contacts');
            }
        }
    })

    .controller('RemoveContactsCtrl', function ($scope, $stateParams, $state, Contacts) {
        $scope.contacts = Contacts.remove($stateParams.contactId);
        $state.go('tab.contacts');//, {reload: true});
    })

    .controller('WeatherCtrl', function ($scope, Weather) {
        $scope.data = {};
        $scope.$watch('data', function (newValue, oldValue) {
            Weather.setCity($scope.data.city);
        }, true);
    })

    .controller('ActualWeatherCtrl', function ($scope, Weather) {
        // Weather.getByCity().then(function (response) {
            // $scope.city = response.data.name;
            // $scope.country = response.data.sys.country;
            // $scope.temp = Math.round(parseFloat(response.data.main.temp) - 273.15);
            // $scope.img = 'http://openweathermap.org/img/w/' + response.data.weather[0].icon + '.png';
            // $scope.background = response.data.weather[0].main.toLowerCase();
            // $scope.pressure = Math.round(parseFloat(response.data.main.pressure));
            // $scope.humidity = response.data.main.humidity;
        // }, function (response) {
            // console.log('error');
        // });
		// Weather.getByCity().then(function(response){
			// $scope.tmp = response;
		// });
		console.log(Weather.getByCity());
    })

    .controller('ForecastWeatherCtrl', function ($scope, Weather) {
        Weather.getForecast().then(function (response) {
            var x2js = new X2JS();
            var json = x2js.xml_str2json(response.data);//JSON.stringify(x2js.xml_str2json(response.data));
            console.log(json.weatherdata.location.name);
            $scope.city = json.weatherdata.location.name;
            $scope.weather = json.weatherdata.forecast.time;
        }, function (response) {
            console.log('error');
        });
    })

    .controller('GalleryCtrl', function ($scope, $ionicBackdrop, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate) {
        var photos = [];
        for (var i = 1; i <= 15; i++) {
            var n = i.toString();
            if (i < 10) {
                n = '0' + n;
            }
            var photo = {
                src: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '.jpg',
                min: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '_min.jpg'
            };
            photos.push(photo);
        }

        $scope.images = photos;
        $scope.zoomMin = 1;

        $scope.showImages = function (index) {
            $scope.activeSlide = index;
            $scope.showModal('templates/gallery/zoomview.html');
        };

        $scope.showModal = function (templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };

        $scope.closeModal = function () {
            $scope.modal.hide();
            $scope.modal.remove()
        };

        $scope.updateSlideStatus = function (slide) {
            var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
            if (zoomFactor == $scope.zoomMin) {
                $ionicSlideBoxDelegate.enableSlide(true);
            } else {
                $ionicSlideBoxDelegate.enableSlide(false);
            }
        };
    })

    .controller('MultimediaCtrl', function ($scope) {
    })

    .controller('AudioCtrl', function ($scope) {
        $scope.tracks = [
            {
                url: 'http://www.bartlomiej-zmudzinski.pl/magisterka/multimedia/Scott%20Buckley%20-%20Wonderful.mp3',
                artist: 'Scott Buckley',
                title: 'Wonderful'
            },
            {
                url: 'http://www.bartlomiej-zmudzinski.pl/magisterka/multimedia/Scott%20Buckley%20-%20Rainbows.mp3',
                artist: 'Scott Buckley',
                title: 'Rainbows'
            }
        ];
    })

    .controller('VideoCtrl', function ($scope) {
    })

    .controller('MapCtrl', function ($scope, $ionicLoading, $cordovaGeolocation) {
        function onDeviceReady() {
            var position = new google.maps.LatLng(51.1086726, 17.0601578); //PWr Location
            var mapOptions = {
                streetViewControl: true,
                center: position,
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.TERRAIN
            };
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            // Try HTML5 geolocation
            if (navigator.geolocation) {
                console.log("Urządzenie wspiera geolokalizację!");
                navigator.geolocation.getCurrentPosition(function (pos) {
                    var pos = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    $scope.map.setCenter(pos);
                    var myLocation = new google.maps.Marker({
                        map: map,
                        animation: google.maps.Animation.BOUNCE,
                        position: pos
                    });
                });
            } else {
                console.log("Urządzenie nie wspiera geolokalizacji...");
            }

            if (window.cordova) {
                console.log("Cordova running");
                var posOptions = {timeout: 20000, enableHighAccuracy: true};
                $cordovaGeolocation.getCurrentPosition(posOptions).then(function (pos) {
                    var pos = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    $scope.map.setCenter(pos);
                    var myLocation = new google.maps.Marker({
                        map: map,
                        animation: google.maps.Animation.BOUNCE,
                        position: pos
                    });
                }, function (err) {
                    console.log('Cordova error: '+err);
                });
            }

            $scope.map = map;
        }
        ionic.Platform.ready(onDeviceReady);
        document.addEventListener("deviceready", onDeviceReady, false);
    })

    .controller('LoginCtrl', function ($scope) {
    });