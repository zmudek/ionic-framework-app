angular.module('starter.services', [])

    .factory('Contacts', function () {
        var contacts = [{
			id: 0,
			name: 'Piotr',
			surname: 'Nowak',
			phone: 123456789,
			email: 'abc@cba.pl',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/1.jpg'
		}, {
			id: 1,
			name: 'Jan',
			surname: 'Kowalski',
			phone: 111222333,
			email: '',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/2.jpg'
		}, {
			id: 2,
			name: 'Adam',
			surname: 'Parzybut',
			phone: 222444666,
			email: '',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/3.jpg'
		}, {
			id: 3,
			name: 'Stefan',
			surname: 'Sosnowski',
			phone: 999888777,
			email: 'a@a.aa',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/whois.png'
		}, {
			id: 4,
			name: 'Krzysztof',
			surname: 'Tokarek',
			phone: 987654321,
			email: 'xyz@wp.pl',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/4.jpg'
		}, {
			id: 5,
			name: 'Mateusz',
			surname: 'Perka',
			phone: 555000555,
			email: 'zzz@gmail.com',
			face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/5.jpg'
		}];
        var id = 5;

        return {
            all: function () {
                return contacts;
            },
            get: function (contactId) {
                for (var i = 0; i < contacts.length; i++) {
                    if (contacts[i].id === parseInt(contactId)) {
                        return contacts[i];
                    }
                }
                return null;
            },
            add: function (contact) {
                var newCon = {
                    id: ++id,
                    name: contact.name,
                    surname: contact.surname,
                    phone: parseInt(contact.phone),
                    email: contact.email,
                    face: ''
                };
                contacts.push(newCon);
            },
            update: function (contact) {
                for (var i = 0; i < contacts.length; i++) {
                    if (contacts[i].id === contact.id) {
                        return contacts[i] = contact;
                    }
                }
                return null;
            },
            remove: function (contactId) {
                var tmp = contacts.filter(function(item) {
                    return item['id'] !== parseInt(contactId);
                });
                contacts = tmp;
                return contacts;
            }
        };
    })

    .factory('Weather', function ($http) {
        var API_PATH = 'http://api.openweathermap.org/data/2.5/weather?appid=d9c18ced2f48846634dcde751ec21134&q=';
        var city = 'Wieluń';
        return {
            setCity: function (newCity) {
                city = newCity;
            },
            getByCity: function () {
                return promise = $http.get(API_PATH + city);
                // success(function(data, status, headers, config) {
                // return data;
                // }).
                // error(function(data, status, headers, config) {
                // return "error" + status;
                // });
            },
            getForecast: function () {
                return promise = $http.get("http://api.openweathermap.org/data/2.5/forecast/daily?appid=d9c18ced2f48846634dcde751ec21134&q="+city+"&mode=xml&units=metric&cnt=16");
            }
        };
    });