// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'ionic-audio'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })

            // Each tab has its own nav history stack:

            .state('tab.contacts', {
                url: '/contacts',
                cache: false,
                views: {
                    'tab-contacts': {
                        templateUrl: 'templates/tab-contacts.html',
                        controller: 'ContactsCtrl'
                    }
                }
            })
            .state('tab.contacts-add', {
                url: '/contacts/add',
                cache: false,
                views: {
                    'tab-contacts': {
                        templateUrl: 'templates/contacts/add.html',
                        controller: 'AddContactsCtrl'
                    }
                }
            })
            .state('tab.contacts-detail', {
                url: '/contacts/detail/:contactId',
                views: {
                    'tab-contacts': {
                        templateUrl: 'templates/contacts/detail.html',
                        controller: 'DetailContactsCtrl'
                    }
                }
            })
            .state('tab.contacts-edit', {
                url: '/contacts/edit/:contactId',
                views: {
                    'tab-contacts': {
                        templateUrl: 'templates/contacts/add.html',
                        controller: 'EditContactsCtrl'
                    }
                }
            })

            .state('tab.weather', {
                url: '/weather',
                views: {
                    'tab-weather': {
                        templateUrl: 'templates/tab-weather.html',
                        controller: 'WeatherCtrl'
                    }
                }
            })
            .state('tab.weather-actual', {
                url: '/weather/actual',
                views: {
                    'tab-weather': {
                        templateUrl: 'templates/weather/actual.html',
                        controller: 'ActualWeatherCtrl'
                    }
                }
            })
            .state('tab.weather-forecast', {
                url: '/weather/forecast',
                views: {
                    'tab-weather': {
                        templateUrl: 'templates/weather/forecast.html',
                        controller: 'ForecastWeatherCtrl'
                    }
                }
            })

            .state('tab.gallery', {
                url: '/gallery',
                views: {
                    'tab-gallery': {
                        templateUrl: 'templates/tab-gallery.html',
                        controller: 'GalleryCtrl'
                    }
                }
            })

            .state('tab.multimedia', {
                url: '/multimedia',
                views: {
                    'tab-multimedia': {
                        templateUrl: 'templates/tab-multimedia.html',
                        controller: 'MultimediaCtrl'
                    }
                }
            })
            .state('tab.multimedia-audio', {
                url: '/multimedia/audio',
                views: {
                    'tab-multimedia': {
                        templateUrl: 'templates/multimedia/audio.html',
                        controller: 'AudioCtrl'
                    }
                }
            })
            .state('tab.multimedia-video', {
                url: '/multimedia/video',
                views: {
                    'tab-multimedia': {
                        templateUrl: 'templates/multimedia/video.html',
                        controller: 'VideoCtrl'
                    }
                }
            })

            .state('tab.map', {
                url: '/map',
                views: {
                    'tab-map': {
                        templateUrl: 'templates/tab-map.html',
                        controller: 'MapCtrl'
                    }
                }
            });

            // .state('tab.login', {
                // url: '/login',
                // views: {
                    // 'tab-login': {
                        // templateUrl: 'templates/tab-login.html',
                        // controller: 'LoginCtrl'
                    // }
                // }
            // });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/contacts');
        $ionicConfigProvider.tabs.position('bottom');

    });